//Funciones

function Promedio(array){
    var Promedio = 0; //inicializar variables
    for(var i= 0; i < array.length; i++){
        Promedio += array[i];
    }
    return Promedio/array.length;
}

function Pares(array){
    var Pares = 0;
    for(var i= 0; i <array.length; i++){
        //condicional de pares
        if(array[i] % 2 == 0){
            Pares++;
        }
    }
    return Pares;
}

function Ordenados(array){
    let ordenado = array.slice(); //  slice :pasar info de un arreglo a una variable
	return ordenado.sort((a, b) => b - a); // sort : ordenar 
}

//FUNCION MOSTRAR ----------------------------
function Mostrar(){
    //Arreglo con valores de 20 posiciones 
    const randomArray = [];
    //ciclo que lanza valores aleatorios
		for (let i = 0; i < 20; i++) {
			randomArray.push(Math.floor(Math.random() * (100 - 1) + 1));
		}
    //imprimir valores de funciones
    document.getElementById('pares').value = Pares(randomArray);
    document.getElementById('promedio').value = Promedio(randomArray);
    document.getElementById('ordenado').value = Ordenados(randomArray);
    document.getElementById('original').value = randomArray;
}

Mostrar();
document.getElementById("BtnCalcular").addEventListener('click', Mostrar);

